stage { 'pre': }
stage { 'repo': }
Stage[repo] -> Stage[pre] -> Stage[main]

class { 'yum':
  extrarepo => [ 'epel' , 'puppetlabs', 'remi', 'remi_php55' ],
  stage => 'repo'
}

##########################
# Environment specific settings
# enshure all this is done in pre Stage
class environment { 

  #default path for exec
  Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }

  # fix some rights for user to access apache and sbk dirs
  $ssh_username = "vagrant"
  $sbk_app_basepath = '/shared/apps/sbk/'
  $sbk_app_path = "${sbk_app_basepath}/current"
  group { 'puppet':   ensure => present }
  group { 'apache':   ensure => present }
  user { $ssh_username:
    groups => ['apache'],
    membership => 'minimum'
  }
  file { "/home/${ssh_username}":
      ensure => directory,
      owner  => $ssh_username,
  }

  # copy dot files to ssh user's home directory
  exec { 'dotfiles':
    cwd     => "/home/${ssh_username}",
    command => "cp -r /vagrant/puppet/files/dot/.[a-zA-Z0-9]* /home/${ssh_username}/ \
                && chown -R ${ssh_username} /home/${ssh_username}/.[a-zA-Z0-9]* \
                && cp -r /vagrant/puppet/files/dot/.[a-zA-Z0-9]* /root/",
    onlyif  => 'test -d /vagrant/puppet/files/dot',
    returns => [0, 1],
  }

  #install some packages
  package {
    ['vim-enhanced', 
    'tree', 
    'ngrep']:
    ensure => present,
  }

  # # local certificate 
  openssl::certificate::x509 { 'local.cornelsen.de':
    ensure       => present,
    country      => 'DE',
    organization => 'developer.cornelsen.de',
    commonname  => 'local.cornelsen.de',
    state        => 'Berlin',
    locality     => 'House 2',
    unit         => 'Development',
    email        => 'juergen.messner@cornelsen',
    days         => 3456,
    base_dir     => '/etc/ssl/certs/',
  }

  include git
}
class { 'environment':
  stage    => 'pre',
}

###########################
# Install all Services and Tools

class { 'nginx':  
  config_file_default_purge => true,
}
file { '/etc/nginx/conf.d':
  ensure => 'directory',
  source => '/vagrant/puppet/files/nginx/conf.d',
  recurse => true,
  notify => Service['nginx', 'php-fpm']
}
service   { 'php-fpm':
  ensure => running,
}
# update /etc/hosts
host { 'phalcon.local':
    ensure => 'present',       
    target => '/etc/hosts',    
    ip => '127.0.0.1',         
}

#deactivate firewall
class { 'iptables':
  disable => true,
  config => 'file',
}

# install php with modules
class { 'php':   
  service => 'nginx'
}
php::module { [
  "mbstring",
  "mcrypt",
  "mysql",
  "fpm"
  ]:
  module_prefix => "php-"
}

php::pear::module { 'phpunit':
  repository  => 'pear.phpunit.de',
  alldeps => true,
  module_prefix => 'phpunit/',
  use_package => false
}
php::pecl::module { [
  "xdebug",
  "apcu",
  "msgpack",
  ]: 
  use_package => 'yes',
  verbose => true,
  prefix => 'php-pecl-'
}
class { 'composer': }
# phalconphp module not working - see shell provisioner in Vagrantfile
# class { 'phalconphp': 
#   ensure => 'master'
# }

# mysql db
class { "mysql":
  root_password => 'vagrant',
}