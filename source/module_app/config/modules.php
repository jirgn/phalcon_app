<?php

/**
 * Register application modules
 */
$application->registerModules(array(
    'frontend' => array(
        'className' => 'Module_app\Frontend\Module',
        'path' => __DIR__ . '/../apps/frontend/Module.php'
    )
));
